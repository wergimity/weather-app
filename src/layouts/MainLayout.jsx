import React from 'react'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faHome, faMap } from '@fortawesome/free-solid-svg-icons'
import { connect } from 'react-redux'
import { selectLocationCoordinates } from '../store/location'
import './MainLayout.scss'

export const MainLayout = connect(
  // State
  (state) => {
    return {
      showMap: !!selectLocationCoordinates(state),
    }
  },
)(
  // Compnnent
  (props) => {
    return (
      <div className="main-layout">
        <nav className="main-layout-nav">
          <Link to="/">
            <FontAwesomeIcon icon={faHome} />
          </Link>
          <ul>
            <li>
              <Link to="/countries">Locations</Link>
            </li>
            {props.showMap && (
              <li>
                <Link to="/map">
                  <FontAwesomeIcon icon={faMap} />
                </Link>
              </li>
            )}
            <li>
              <Link to="/search">
                <FontAwesomeIcon icon={faSearch} />
              </Link>
            </li>
          </ul>
        </nav>
        {props.children}
        {props.loading && <div>Loading...</div>}
      </div>
    )
  },
)
