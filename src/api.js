import axios from 'axios'

export const api = axios.create({
  baseURL: 'http://api.openweathermap.org/data/2.5',
})

api.defaults.params = { appid: process.env.REACT_APP_API_KEY, units: 'metric' }
