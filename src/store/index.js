import { connectRouter, routerMiddleware } from 'connected-react-router'
import { createBrowserHistory } from 'history'
import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import thunk from 'redux-thunk'
import { api } from '../api'
import citiesReducer, { CityActionTypes } from './cities'
import countriesReducer from './countries'
import locationReducer from './location'
import mapReducer from './map'
import weatherReducer from './weather'
import favoritesReducer from './favorites'

export const dependencies = {
  api,
  history: createBrowserHistory(),
}

export function configureStore() {
  const composeEnhancers =
    process.env.NODE_ENV === 'development' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
          actionSanitizer: (action) => {
            switch (action.type) {
              case CityActionTypes.LOAD_CITIES_SUCCESS:
                return { ...action, payload: `'<<${action.payload.length} CITIES>>'` }
              default:
                return action
            }
          },
          stateSanitizer: (state) => {
            return {
              ...state,
              cities: {
                ...state.cities,
                data: state.cities.data ? `<<${state.cities.data.length} CITIES>>` : null,
              },
            }
          },
        })
      : compose

  const store = createStore(
    combineReducers({
      router: connectRouter(dependencies.history),
      countries: countriesReducer,
      cities: citiesReducer,
      location: locationReducer,
      weather: weatherReducer,
      map: mapReducer,
      favorites: favoritesReducer,
    }),
    composeEnhancers(
      applyMiddleware(thunk.withExtraArgument(dependencies)), //
      applyMiddleware(routerMiddleware(dependencies.history)),
    ),
  )

  return store
}
