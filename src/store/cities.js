import axios from 'axios'
import { createSelector } from 'reselect'
import { keyBy } from 'lodash-es'
import qs from 'qs'
import { selectCountryMap } from './countries'

export const CityActionTypes = {
  LOAD_CITIES: 'app/cities/LOAD_CITIES',
  LOAD_CITIES_SUCCESS: 'app/cities/LOAD_CITIES_SUCCESS',
  LOAD_CITIES_FAILED: 'app/cities/LOAD_CITIES_FAILED',
}
export const initialCitiesState = {
  loading: false,
  data: null,
  error: null,
}
export default function citiesReducer(state = initialCitiesState, action) {
  switch (action.type) {
    case CityActionTypes.LOAD_CITIES:
      return { ...state, loading: true }
    case CityActionTypes.LOAD_CITIES_SUCCESS:
      return { ...state, loading: false, data: action.payload, error: null }
    case CityActionTypes.LOAD_CITIES_FAILED:
      return { ...state, loading: false, data: null, error: action.payload }
    default:
      return state
  }
}
export const loadCities = () => async (dispatch) => {
  try {
    dispatch({ type: CityActionTypes.LOAD_CITIES })
    const { data: payload } = await axios.get('city.list.min.json')
    dispatch({ type: CityActionTypes.LOAD_CITIES_SUCCESS, payload })
    return true
  } catch (payload) {
    console.error(payload)
    dispatch({ type: CityActionTypes.LOAD_CITIES_FAILED, payload })
    return false
  }
}

export const selectCityState = (state) => state.cities || initialCitiesState
export const selectQuery = (state, search) => {
  return search ? qs.parse(search.substring(1)).query || '' : ''
}
export const selectCountryParam = (state, country) => country
export const selectCitiesLoading = createSelector(
  selectCityState,
  (state) => state.loading,
)
export const selectCityList = createSelector(
  selectCityState,
  (state) => state.data || [],
)
export const selectCityMap = createSelector(
  selectCityList,
  (cities) => keyBy(cities, 'id'),
)
export const selectCountryCityList = createSelector(
  selectCityList,
  selectCountryParam,
  (cities, country) => cities.filter((x) => x.country === country),
)
export const selectFilteredCityList = createSelector(
  selectCityList,
  selectQuery,
  selectCountryMap,
  (cities, query, countries) =>
    query.length < 3
      ? []
      : cities //
          .filter((x) => x.name.toLowerCase().includes(query.toLowerCase()))
          .slice(0, 100)
          .map((city) => {
            return {
              ...city,
              country: countries[city.country] || null,
            }
          }),
)
