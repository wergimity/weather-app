import axios from 'axios'
import { createSelector } from 'reselect'
import { keyBy } from 'lodash-es'

export const CountryActionTypes = {
  LOAD_COUNTRIES: 'app/countries/LOAD_COUNTRIES',
  LOAD_COUNTRIES_SUCCESS: 'app/countries/LOAD_COUNTRIES_SUCCESS',
  LOAD_COUNTRIES_FAILED: 'app/countries/LOAD_COUNTRIES_FAILED',
}

export const initialCountriesState = {
  loading: false,
  data: null,
  error: null,
}

export default function countriesReducer(state = initialCountriesState, action) {
  switch (action.type) {
    case CountryActionTypes.LOAD_COUNTRIES:
      return { ...state, loading: true }
    case CountryActionTypes.LOAD_COUNTRIES_SUCCESS:
      return { ...state, loading: false, data: action.payload, error: null }
    case CountryActionTypes.LOAD_COUNTRIES_FAILED:
      return { ...state, loading: false, data: null, error: action.payload }
    default:
      return state
  }
}

export const loadCountries = () => async (dispatch) => {
  try {
    dispatch({ type: CountryActionTypes.LOAD_COUNTRIES })
    const { data: payload } = await axios.get('country.list.min.json')
    dispatch({ type: CountryActionTypes.LOAD_COUNTRIES_SUCCESS, payload })
    return true
  } catch (payload) {
    console.error(payload)
    dispatch({ type: CountryActionTypes.LOAD_COUNTRIES_FAILED, payload })
    return false
  }
}

export const selectCountryState = (state) => state.countries || initialCountriesState
export const selectCountriesLoading = createSelector(
  selectCountryState,
  (state) => state.loading,
)
export const selectCountryList = createSelector(
  selectCountryState,
  (state) => state.data || [],
)
export const selectCountryMap = createSelector(
  selectCountryList,
  (countries) => keyBy(countries, 'code'),
)
