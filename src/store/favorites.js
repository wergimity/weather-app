import { createSelector } from 'reselect'

export const FavoritesActionTypes = {
  RESTORE_FAVORITES: '@@INIT',
  ADD_FAVORITE: 'app/favorites/ADD_FAVORITE',
  REMOVE_FAVORITE: 'app/favorites/REMOVE_FAVORITE',
}

export const initialFavoritesState = {
  favorites: {},
}

export default function favoritesReducer(state = initialFavoritesState, action) {
  switch (action.type) {
    case FavoritesActionTypes.RESTORE_FAVORITES:
      const restored = {}
      for (let i = 0; i < localStorage.length; i++) {
        const key = localStorage.key(i)
        const match = key.match(/app_favorite_(\d+)/)
        if (!match) continue
        restored[match[1]] = JSON.parse(localStorage.getItem(key))
      }
      return { ...state, favorites: restored }
    case FavoritesActionTypes.ADD_FAVORITE:
      localStorage.setItem(`app_favorite_${action.payload.id}`, JSON.stringify(action.payload))
      return { ...state, favorites: { ...state.favorites, [action.payload.id]: action.payload } }
    case FavoritesActionTypes.REMOVE_FAVORITE:
      localStorage.removeItem(`app_favorite_${action.payload}`)
      const removed = { ...state.favorites }
      delete removed[action.payload]
      return { ...state, favorites: removed }
    default:
      return state
  }
}

export const addFavorite = (weather) => async (dispatch) => {
  if (!weather || !weather.city || !weather.country) {
    return false
  }

  dispatch({
    type: FavoritesActionTypes.ADD_FAVORITE,
    payload: {
      id: weather.city.id,
      cityName: weather.city.name,
      country: weather.country.code,
      countryName: weather.country.country,
    },
  })
  return true
}

export const removeFavorite = (weather) => async (dispatch) => {
  if (!weather || !weather.city || !weather.country) {
    return false
  }

  dispatch({
    type: FavoritesActionTypes.REMOVE_FAVORITE,
    payload: weather.city.id,
  })
  return true
}

export const selectFavoritesState = (state) => state.favorites || initialFavoritesState
export const selectId = (state, id) => id
export const selectFavoriteMap = createSelector(
  selectFavoritesState,
  (state) => state.favorites,
)
export const selectIsFavorite = createSelector(
  selectFavoriteMap,
  selectId,
  (favorites, id) => !!favorites[id],
)
export const selectFavoritesList = createSelector(
  selectFavoriteMap,
  (favorites) => Object.values(favorites),
)
