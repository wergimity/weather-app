import { createSelector } from 'reselect'
import { selectCityMap } from './cities'
import { selectCountryMap } from './countries'

// import { createSelector } from 'reselect'

export const WeatherActionTypes = {
  LOAD_WEATHER: 'app/weatcher/LOAD_WEATHER',
  LOAD_WEATHER_SUCCESS: 'app/weatcher/LOAD_WEATHER_SUCCESS',
  LOAD_WEATHER_FAILED: 'app/weatcher/LOAD_WEATHER_FAILED',
  CLEAR_WEATHER: 'app/weather/CLEAR_WEATHER',
}

export const initialWeatherState = {
  loading: false,
  data: null,
  error: null,
}

export default function weatherReducer(state = initialWeatherState, action) {
  switch (action.type) {
    case WeatherActionTypes.LOAD_WEATHER:
      return { ...state, loading: true }
    case WeatherActionTypes.LOAD_WEATHER_SUCCESS:
      return { ...state, loading: false, data: action.payload, error: null }
    case WeatherActionTypes.LOAD_WEATHER_FAILED:
      return { ...state, loading: false, data: null, error: action.payload }
    case WeatherActionTypes.CLEAR_WEATHER:
      return { ...state, loading: false, data: null, error: null }
    default:
      return state
  }
}

export const loadWeatherByCoordinates = (coordinates) => async (dispatch, getState, { api }) => {
  try {
    dispatch({ type: WeatherActionTypes.LOAD_WEATHER })
    const { data: payload } = await api.get('weather', { params: coordinates })
    dispatch({ type: WeatherActionTypes.LOAD_WEATHER_SUCCESS, payload })
  } catch (payload) {
    console.error(payload)
    dispatch({ type: WeatherActionTypes.LOAD_WEATHER_FAILED, payload })
  }
}

export const loadWeatherByLocation = (location) => async (dispatch, getState, { api }) => {
  try {
    dispatch({ type: WeatherActionTypes.LOAD_WEATHER })
    const { data: payload } = await api.get('weather', { params: { id: location } })
    dispatch({ type: WeatherActionTypes.LOAD_WEATHER_SUCCESS, payload })
  } catch (payload) {
    console.error(payload)
    dispatch({ type: WeatherActionTypes.LOAD_WEATHER_FAILED, payload })
  }
}

export const clearWeather = () => async (dispatch) => {
  dispatch({ type: WeatherActionTypes.CLEAR_WEATHER })
}

export const selectWeatherState = (state) => state.weather || initialWeatherState
export const selectWeatherLoading = createSelector(
  selectWeatherState,
  (state) => state.loading,
)
export const selectWeatherData = createSelector(
  selectWeatherState,
  (state) => state.data,
)
export const selectCombinedWeatherData = createSelector(
  selectWeatherData,
  selectCityMap,
  selectCountryMap,
  (weather, cities, countries) => {
    const city = weather ? cities[weather.id] : null
    const country = city ? countries[city.country] : null
    return weather
      ? {
          ...weather,
          city,
          country,
          weather: {
            ...weather.weather[0],
            iconURL: `http://openweathermap.org/img/w/${weather.weather[0].icon}.png`,
          },
        }
      : null
  },
)
