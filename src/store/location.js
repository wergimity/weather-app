import { createSelector } from 'reselect'

// import { createSelector } from 'reselect'

export const LocationActionTypes = {
  LOAD_LOCATION: 'app/location/LOAD_LOCATION',
  LOAD_LOCATION_SUCCESS: 'app/location/LOAD_LOCATION_SUCCESS',
  LOAD_LOCATION_FAILED: 'app/location/LOAD_LOCATION_FAILED',
}

export const initialLocationState = {
  loading: false,
  data: null,
  error: null,
  location: null,
}

export default function locationReducer(state = initialLocationState, action) {
  switch (action.type) {
    case LocationActionTypes.LOAD_LOCATION:
      return { ...state, loading: true }
    case LocationActionTypes.LOAD_LOCATION_SUCCESS:
      return { ...state, loading: false, data: action.payload, error: null }
    case LocationActionTypes.LOAD_LOCATION_FAILED:
      return { ...state, loading: false, data: null, error: action.payload }
    default:
      return state
  }
}

export const loadLocation = () => async (dispatch) => {
  try {
    dispatch({ type: LocationActionTypes.LOAD_LOCATION })

    if (!navigator.geolocation) throw new Error('Geolocation is not abailable')

    const payload = await new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        (location) => {
          const { latitude, longitude } = location.coords
          resolve({ latitude, longitude })
        },
        (error) => {
          reject(error)
        },
      )
    })

    dispatch({ type: LocationActionTypes.LOAD_LOCATION_SUCCESS, payload })
    return true
  } catch (payload) {
    console.error(payload)
    dispatch({ type: LocationActionTypes.LOAD_LOCATION_FAILED, payload })
    return false
  }
}

export const selectLocationState = (state) => state.location || initialLocationState
export const selectLocationLoading = createSelector(
  selectLocationState,
  (state) => state.loading,
)
export const selectLocationCoordinates = createSelector(
  selectLocationState,
  (state) => (state.data ? { lat: state.data.latitude, lon: state.data.longitude } : null),
)
export const selectLocationError = createSelector(
  selectLocationState,
  (state) => !!state.error,
)
