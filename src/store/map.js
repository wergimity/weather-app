import { createSelector } from 'reselect'

export const MapActionTypes = {
  LOAD_MAP: 'app/map/LOAD_MAP',
  LOAD_MAP_SUCCESS: 'app/map/LOAD_MAP_SUCCESS',
  LOAD_MAP_FAILED: 'app/map/LOAD_MAP_FAILED',
}

export const initialMapState = {
  loading: false,
  data: null,
  error: null,
}

export default function mapReducer(state = initialMapState, action) {
  switch (action.type) {
    case MapActionTypes.LOAD_MAP:
      return { ...state, loading: true }
    case MapActionTypes.LOAD_MAP_SUCCESS:
      return { ...state, loading: false, data: action.payload, error: null }
    case MapActionTypes.LOAD_MAP_FAILED:
      return { ...state, loading: false, data: null, error: action.payload }
    default:
      return state
  }
}

export const loadMapWeather = (bbox) => async (dispatch, getState, { api }) => {
  try {
    dispatch({ type: MapActionTypes.LOAD_MAP })
    const { data: payload } = await api.get('box/city', { params: { bbox } })
    dispatch({ type: MapActionTypes.LOAD_MAP_SUCCESS, payload })
    return true
  } catch (payload) {
    console.error(payload)
    dispatch({ type: MapActionTypes.LOAD_MAP_FAILED })
    return false
  }
}

export const selectMapState = (state) => state.map || initialMapState
export const selectMapLoading = createSelector(
  selectMapState,
  (state) => state.loading,
)
export const selectMapData = createSelector(
  selectMapState,
  (state) => state.data || { list: [] },
)
export const selectMapList = createSelector(
  selectMapData,
  (data) => data.list,
)
