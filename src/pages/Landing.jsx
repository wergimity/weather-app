import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { Weather } from '../components/Weather'
import { MainLayout } from '../layouts/MainLayout'
import { selectCitiesLoading } from '../store/cities'
import { selectCountriesLoading } from '../store/countries'
import {
  selectLocationCoordinates,
  selectLocationLoading,
  selectLocationError,
} from '../store/location'
import {
  clearWeather,
  loadWeatherByCoordinates,
  selectCombinedWeatherData,
  selectWeatherLoading,
} from '../store/weather'
import { selectIsFavorite, addFavorite, removeFavorite } from '../store/favorites'

export const LandingPage = connect(
  // State
  (state) => {
    const weather = selectCombinedWeatherData(state)
    return {
      weather,
      loading: selectLocationLoading(state) || selectWeatherLoading(state),
      loadingLocation: selectCitiesLoading(state) || selectCountriesLoading(state),
      coordinates: selectLocationCoordinates(state),
      locationBlocked: selectLocationError(state),
      isFavorite: selectIsFavorite(state, weather && weather.city ? weather.city.id : null),
    }
  },
  // Dispatch
  { loadWeatherByCoordinates, clearWeather, addFavorite, removeFavorite },
)(
  // Component
  (props) => {
    useEffect(() => {
      if (props.coordinates) {
        props.loadWeatherByCoordinates(props.coordinates)
      }
      return () => props.clearWeather()
    }, [props.coordinates])

    return (
      <MainLayout loading={props.loading}>
        <div className="landing">
          <Weather
            weather={props.weather}
            loadingLocation={props.loadingLocation}
            isFavorite={props.isFavorite}
            onAddFavorite={() => props.addFavorite(props.weather)}
            onRemoveFavorite={() => props.removeFavorite(props.weather)}
          />
          {props.locationBlocked && <div>Please enable location.</div>}
        </div>
        <pre>{JSON.stringify(props.weather, null, 2)}</pre>
      </MainLayout>
    )
  },
)
