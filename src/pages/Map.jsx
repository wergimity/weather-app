import React from 'react'
import { connect } from 'react-redux'
import { MainLayout } from '../layouts/MainLayout'
import { Map } from '../components/Map'
import {
  selectLocationLoading,
  selectLocationCoordinates,
  selectLocationError,
} from '../store/location'
import { loadMapWeather, selectMapList } from '../store/map'
import { Redirect } from 'react-router-dom'

export const MapPage = connect(
  // State
  (state, props) => {
    return {
      loading: selectLocationLoading(state),
      coordinates: selectLocationCoordinates(state),
      list: selectMapList(state),
      locationBlocked: selectLocationError(state),
    }
  },
  // Dispatch
  { loadMapWeather },
)(
  // Component
  (props) => {
    if (props.locationBlocked) {
      return <Redirect to="/" />
    }
    return (
      <MainLayout loading={props.loading}>
        {!!props.coordinates && (
          <Map
            center={props.coordinates}
            list={props.list.map((x) => ({
              id: x.id,
              lat: x.coord ? x.coord.Lat : null,
              lng: x.coord ? x.coord.Lon : null,
              icon: x.weather && x.weather[0] ? x.weather[0].icon : null,
            }))}
            onBoundsChanged={(bbox) => {
              props.loadMapWeather(bbox)
            }}
          />
        )}
      </MainLayout>
    )
  },
)
