import React from 'react'
import { connect } from 'react-redux'
import { List } from '../components/List'
import { MainLayout } from '../layouts/MainLayout'
import { selectCitiesLoading, selectCountryCityList } from '../store/cities'

export const CitiesPage = connect(
  // State
  (state, props) => {
    return {
      loading: selectCitiesLoading(state),
      cities: selectCountryCityList(state, props.match.params.country),
    }
  },
)(
  // Component
  (props) => {
    return (
      <MainLayout loading={props.loading}>
        <div className="cities">
          <List
            items={props.cities.map((x) => ({
              id: x.id,
              to: `/weather/${x.id}`,
              label: x.name,
            }))}
          />
        </div>
      </MainLayout>
    )
  },
)
