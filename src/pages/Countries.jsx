import React from 'react'
import { connect } from 'react-redux'
import { List } from '../components/List'
import { MainLayout } from '../layouts/MainLayout'
import { loadCountries, selectCountriesLoading, selectCountryList } from '../store/countries'
import { selectFavoritesList } from '../store/favorites'
export const CountriesPage = connect(
  // State
  (state) => {
    return {
      loading: selectCountriesLoading(state),
      countries: selectCountryList(state),
      favorites: selectFavoritesList(state),
    }
  },
  // Dispatch
  { loadCountries },
)(
  // Component
  (props) => {
    return (
      <MainLayout loading={props.loading}>
        <div className="countries">
          {!!props.favorites.length && (
            <>
              <div>
                <small>Favorites</small>
              </div>
              <List
                items={props.favorites.map((x) => ({
                  id: x.id,
                  to: `/weather/${x.id}`,
                  label: `${x.cityName} (${x.countryName})`,
                }))}
              />
            </>
          )}
          <div>
            <small>Countries</small>
          </div>
          <List
            items={props.countries.map((x) => ({
              id: x.code,
              to: `/countries/${x.code}`,
              label: x.country,
            }))}
          />
        </div>
      </MainLayout>
    )
  },
)
