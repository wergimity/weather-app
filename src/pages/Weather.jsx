import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { Weather } from '../components/Weather'
import { MainLayout } from '../layouts/MainLayout'
import { selectCitiesLoading } from '../store/cities'
import { selectCountriesLoading } from '../store/countries'
import {
  clearWeather,
  loadWeatherByLocation,
  selectCombinedWeatherData,
  selectWeatherLoading,
} from '../store/weather'
import { selectIsFavorite, addFavorite, removeFavorite } from '../store/favorites'

export const WeatherPage = connect(
  // State
  (state, props) => {
    const weather = selectCombinedWeatherData(state)
    return {
      weather,
      loading: selectWeatherLoading(state),
      loadingLocation: selectCitiesLoading(state) || selectCountriesLoading(state),
      location: props.match.params.location,
      isFavorite: selectIsFavorite(state, weather && weather.city ? weather.city.id : null),
    }
  },
  // Dispatch
  { loadWeatherByLocation, clearWeather, addFavorite, removeFavorite },
)(
  // Component
  (props) => {
    useEffect(() => {
      if (props.location) {
        props.loadWeatherByLocation(props.location)
      }
      return () => props.clearWeather()
    }, [props.location])

    return (
      <MainLayout loading={props.loading}>
        <div className="weather-page">
          <Weather
            weather={props.weather}
            loadingLocation={props.loadingLocation}
            isFavorite={props.isFavorite}
            onAddFavorite={() => props.addFavorite(props.weather)}
            onRemoveFavorite={() => props.removeFavorite(props.weather)}
          />
        </div>
      </MainLayout>
    )
  },
)
