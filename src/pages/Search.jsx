import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import * as qs from 'qs'
import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { List } from '../components/List'
import { MainLayout } from '../layouts/MainLayout'
import { selectCitiesLoading, selectFilteredCityList, selectQuery } from '../store/cities'
import { selectCountriesLoading } from '../store/countries'
import './Search.scss'

export const SearchPage = connect(
  // State
  (state, props) => {
    return {
      cities: selectFilteredCityList(state, props.location.search),
      loading: selectCitiesLoading(state) || selectCountriesLoading(state),
      routeQuery: selectQuery(state, props.location.search),
    }
  },
  // Dispatch
  {},
)(
  // Component
  (props) => {
    const [query, setQuery] = useState(props.routeQuery)

    useEffect(() => {
      props.history.replace('?' + qs.stringify({ query }))
    }, [query])

    return (
      <MainLayout loading={props.loading}>
        <div className="search">
          <form onSubmit={(e) => e.preventDefault()}>
            <div>
              <label>Search</label>
              <input
                disabled={props.loading}
                value={query}
                onChange={(e) => setQuery(e.currentTarget.value)}
              />
            </div>
            <button type="submit" disabled={props.loading}>
              <FontAwesomeIcon icon={faSearch} />
            </button>
          </form>
          {!props.loading && (
            <>
              {props.routeQuery.length < 3 ? (
                <p>Enter something to start search.</p>
              ) : (
                <List
                  items={props.cities.map((x) => ({
                    id: x.id,
                    to: `/weather/${x.id}`,
                    label: x.country ? `${x.name} (${x.country.country})` : x.name,
                  }))}
                />
              )}
              {props.routeQuery.length >= 3 && props.cities.length === 0 && (
                <div>No results found!</div>
              )}
            </>
          )}
        </div>
      </MainLayout>
    )
  },
)
