import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { Route, Switch, withRouter } from 'react-router'
import { CitiesPage } from './pages/Cities'
import { CountriesPage } from './pages/Countries'
import { WeatherPage } from './pages/Weather'
import { loadCities } from './store/cities'
import { loadCountries } from './store/countries'
import { loadLocation } from './store/location'
import { LandingPage } from './pages/Landing'
import { SearchPage } from './pages/Search'
import { MapPage } from './pages/Map'

export const App = withRouter(
  connect(
    // State
    () => ({}),
    // Dispatch
    { loadCities, loadCountries, loadLocation },
  )(
    // Component
    (props) => {
      useEffect(() => {
        props.loadCities()
        props.loadCountries()
        props.loadLocation()
      }, [])

      return (
        <Switch>
          <Route path="/" exact component={LandingPage} />
          <Route path="/map" exact component={MapPage} />
          <Route path="/search" exact component={SearchPage} />
          <Route path="/countries" exact component={CountriesPage} />
          <Route path="/countries/:country" exact component={CitiesPage} />
          <Route path="/weather/:location" exact component={WeatherPage} />
        </Switch>
      )
    },
  ),
)
