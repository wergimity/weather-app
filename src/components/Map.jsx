import React, { useRef } from 'react'
import { GoogleMap, Marker, withGoogleMap, withScriptjs } from 'react-google-maps'
import { debounce } from 'lodash-es'

const EMPTY_FN = () => {}

const InnerMap = withScriptjs(
  withGoogleMap(({ center = null, onBoundsChanged = EMPTY_FN, list = [] }) => {
    const mapRef = useRef(null)

    if (!center) {
      return null
    }
    console.log(center)
    return (
      <GoogleMap
        ref={mapRef}
        defaultZoom={8}
        defaultCenter={{ lat: center.lat, lng: center.lon }}
        onBoundsChanged={debounce(() => {
          const { south, west, north, east } = mapRef.current.getBounds().toJSON()
          const zoom = mapRef.current.getZoom()
          onBoundsChanged(`${west},${south},${east},${north},${zoom}`)
        }, 1000)}
      >
        {list.map((x) => (
          <Marker key={x.id} position={x} icon={`http://openweathermap.org/img/w/${x.icon}.png`} />
        ))}
      </GoogleMap>
    )
  }),
)

export const Map = (props) => {
  return (
    <InnerMap
      {...props}
      googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${
        process.env.REACT_APP_GOOGLE_MAPS_KEY
      }`}
      loadingElement={<div style={{ height: `100%` }} />}
      containerElement={<div style={{ height: `400px`, maxWidth: `600px`, margin: 'auto' }} />}
      mapElement={<div style={{ height: `100%` }} />}
    />
  )
}
