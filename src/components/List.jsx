import React from 'react'
import { Link } from 'react-router-dom'
import './List.scss'

export const List = ({ items = [] }) => {
  if (items.length === 0) {
    return null
  }

  return (
    <ul className="list">
      {items.map((item) => (
        <li key={item.id}>
          <Link to={item.to}>{item.label}</Link>
        </li>
      ))}
    </ul>
  )
}
