import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import './Favorite.scss'

const EMPTY_FN = () => {}

export const Favorite = ({ added = false, onAdd = EMPTY_FN, onRemove = EMPTY_FN }) => {
  return (
    <FontAwesomeIcon
      className={`favorite${added ? ' added' : ''}`}
      onClick={added ? onRemove : onAdd}
      icon={faStar}
    />
  )
}
