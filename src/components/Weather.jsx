import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { format } from 'date-fns'
import React from 'react'
import { Favorite } from './Favorite'
import './Weather.scss'

const EMPTY_FN = () => {}

export const Weather = ({
  weather = null, //
  loadingLocation = false,
  isFavorite = false,
  onAddFavorite = EMPTY_FN,
  onRemoveFavorite = EMPTY_FN,
}) => {
  if (!weather) {
    return null
  }
  return (
    <div className="weather">
      {loadingLocation ? (
        <div className="location">Loading...</div>
      ) : (
        <>
          {weather.country && weather.city && (
            <div className="location">
              <FontAwesomeIcon icon={faMapMarkerAlt} />
              <ul>
                <li>{weather.country.country}</li>
                <li>{weather.city.name}</li>
              </ul>
              <Favorite added={isFavorite} onAdd={onAddFavorite} onRemove={onRemoveFavorite} />
            </div>
          )}
        </>
      )}
      <div className="time">{format(weather.dt * 1000, 'yyyy-MM-dd HH:mm')}</div>
      <div className="highlight">
        <img src={weather.weather.iconURL} alt={weather.weather.description} />
        <span>{Math.round(weather.main.temp)}&deg;</span>
      </div>
      <div className="description">{weather.weather.main}</div>
    </div>
  )
}
