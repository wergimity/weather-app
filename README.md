# Weather app

## Setup
* Install project
  ```
  git clone git@gitlab.com:wergimity/weather-app.git
  cd weather-app
  yarn
  ```
* Setup `.env` file with API keys
* Start project
  ```
  yarn start
  ```

## Build
```
yarn build
```